#!/bin/bash
sudo apt-get -y install php7.0 libapache2-mod-php7.0
sudo apt-get install php7.0-mysql php7.0-mcrypt php7.0-curl php-all-dev php7.0-gd php-pear php-imagick php7.0-pspell php7.0-xmlrpc php7.0-mbstring -y
sudo apt-get install phpmyadmin -y